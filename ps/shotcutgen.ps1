Param(
    [String]$Path = '.\sample.ps1'
)
$msg = 'Creating shortcut for script "{0}"' -f $Path
Write-Host $msg
Start-Sleep -Seconds 5


$StartPath = "C:\temp\"
#$Path = ".\ps\second.ps1"

if(-not (Test-Path $Path))
{
    throw "Unable to find $path"
}
$Item = Get-Item $Path


$Desktop = Join-Path $env:USERPROFILE "Desktop"
$ShortcutFilename = 'shortcut_{0}.lnk' -f $item.BaseName
$DestinationShortcut = Join-Path $Desktop $ShortcutFilename
$Shell = New-Object -ComObject ("WScript.Shell")
$ShortCut = $Shell.CreateShortcut($DestinationShortcut)
$ShortCut.TargetPath = "powershell.exe"
$ShortCut.WorkingDirectory = $StartPath
$Arguments = '-noexit -file "{0}"' -f $Item.FullName
$ShortCut.Arguments = $Arguments
$ShortCut.Save()